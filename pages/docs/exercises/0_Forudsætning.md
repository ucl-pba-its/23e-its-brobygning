---
hide:
  - footer
---

# Opgave 0 - Forudsætning

## Information
For at kunne deltage i dagen øvelser, er det en forudsætning af du har følgende installeret på din
pc:  
  
- Wireshark  
- VMware Workstation player  

Herudover skal du hente følgende images:  
  
- Kali Linux  
- Metasploitable 2  

Disse images skal du åbne me VMware workstation, for herefter at kontroller om de er sat op til at 
benytte _NAT_, dette gør du ved at højere klikke på vm'en i Vmware workstation og vælge `settings`.
Ud for _Network Adapter_ bør der stå _NAT_.

Sidst men ikke mindst, bør du havde læst forberedelsen til idag.
