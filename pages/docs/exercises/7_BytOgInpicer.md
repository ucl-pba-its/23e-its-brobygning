---
hide:
  - footer
---

# Opgave 7 - Byt og inspicer

## Information
I denne opgave skal bytte pcap file med optaget netværk trafik med et andet 
hold(Google drive,onedrive eller ligende), og inspicer trafikken fra det andet hold.

## Instruktioner

1. inspicer det andet holds pcap file.
2. Dokumenter eventuelle fund.
3. lav en vidensdeling internt i gruppen, og med det andet hold, hvad fandt i? 
