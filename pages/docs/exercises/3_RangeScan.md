---
hide:
  - footer
---

# Opgave 3 - ip range scan med nmap

## Information
I denne opgave skal der lave et såkaldt _ip range scan_.
_ip range scan_ betyder egentlig bare at man forsøger at få kontakt til
så mange hosts som muligt, på et eller flere subnets.
  
Værktøjet der skal anvendes er [nmap](https://nmap.org/).
Nmap er et værktøj der kan bruges til netværkskortlægning ved sender forspøgelser
til andre hosts på et netværk.

Målet for opgaven er at finde Metasploitable VM'ens ip adresse, ved at anvende range scan.

## Instruktioner
1. Login i kali med kali som brugernavn og adgangskode
2. Lav en inspektion af topmenuen, og benyt “terminal”-ikonet til at åbne en terminal (Eller benyt genvejen ctrl+alt+t)
3. I terminalen, skriv følgende kommando og tryk på enter: `ifconfig`
4. Notér din lokale kali ip adresse
_Hint: den skulle gerne hedder noget med: 192.168.x.x_
5. Benyt derefter denne viden til at lave en range scan med nmap: `nmap 192.168.x.0-255`
6. Notér de øvrige maskiner på dit lokale netværk, især dem der har et udvalg af interessante services
