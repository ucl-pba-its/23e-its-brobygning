---
hide:
  - footer
---

# Opgave 1 - Forforståelsesøvelse

## Information  
Dette er en terminologiøvelse, hvor I (i jeres team) skal afstemme jeres forforståelse for de forskellige termer for dagen.
  
## Instruktioner  
_Tidsrammen for opgaven er Ca. 20 minutter_

1. Del teamet op i to mindre grupper  
2. Brug “Møde på midten” i hver gruppe til at diskutere begreberne “Virtuel maskine”, “Wireshark”, “Metasploit Framework”, “Metasploitable”, “Kali”. Husk, at alle i gruppen skal have taletid.

Tidsrammen for opgaven er Ca. 20 minutter

