---
hide:
  - footer
  - toc
---

# UCL PBa IT Sikkerhed - _Brobygning_

På dette website finder du opgaver til brobygning under fanen _Opgaver_

I fanen øverst oppe 

![Image light theme](images/UCL_horisontal_logo_UK_rgb.png#only-light){ width="300", align=right }
![Image dark theme](images/UCL_horisontal_logo_UK_neg_rgb.png#only-dark){ width="300", align=right }


