---
hide:
  - footer
---

# Opgave 5 - Benyt Metasploit Framework Console 

## Information
I denne opgave skal værktøjet _Metasploit framework console_ anvendes i Linux.

Metasploit konsollen har sin egen database med kendte sårbarheder i software. Denne
kan anvendes i Metasploit konsollen med kommandoen `search <software navn>`. 
Har vi F.eks. opdaget at der kører en samba service, kan vi vælge at undersøge denne 
service for sårbarheder. Her ville vi så eksekver kommandoen `search samba`.
At bare søge på en service kan give meget store resultater. Her kan det være
bedre at specificere den specifikke version af servicen, med kommandoen `search <software navn> <Version>`

[Denne tutorial](https://medium.com/quiknapp/how-to-load-and-use-exploit-in-metasploit-61b4f10ceb9d) giver
en udemærket gennemgang af konceptet, såfremt du skulle ønske at lære mere om _Msf search_

## Instruktioner
Alle instruktioner udføres i Kali.

1. Start Metasploit konsolen med kommandoen `msfconsole`
2. Søg efter kendte sårbarheder for en af de service der blev noteret i forrige opgave. Anvend kommandoen `search <service>`, hvad ser du? er det overskueligt?
3. Søg efter kendte sårbarheder for en specifik version af de services der blev noteret i forrige opgave. Anvend kommandoen `search <service> <Version nummer>`, hvad ser du? er det overskueligt?
4. Undlad at lukke terminalen, da resultat fra forrige trin skal bruges i næste øvelse.
  