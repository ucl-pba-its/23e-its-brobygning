---
hide:
  - footer
---

# Opgave 4 - Specifik version af services

## Information
I forrige opgave blev der udført et fuldt _range scan_. Og ip adressen til 
metasploitable VM'en blev fundet. I denne opgave skal Nmap anvendes målrettet
mod Metasploitable VM'en, til at finde ud hvilken service der er tilgængelige.

notér dem der virker interessante.

## Instruktioner
1. Benyt kommandoen `nmap <lokal-ip-med-services> -A` til at lave en list over tilgængelige services på Metasploitable VM.
2. Noter de services der kunne være intresant ned i notesblok eller ligende.
_Noter gerne flere_
