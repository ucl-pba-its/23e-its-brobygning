---
hide:
  - footer
---

# Opgave 2 - Klargøring

## Information
I denne opgave skal i sikre, at jeres opsætning af begge virtuelle maskiner er korrekt,
samt at i kan starte wireshark og begge virtuelle maskiner.
## Instruktioner
1. Åben VMware Workstation 16 Player, og kontroller at Kali og Metasplotable begge er sat til NAT
_Højreklik på dem enkeltvis, vælg “Settings” og dernæst “Network Adapter”_
2. Åben Kali VMø'en i VMware Workstation 16 Player
3. Åben Metasplotable 2 VM'en i VMware Workstation 16 Player
_Gratis versionen af vmware understøtter kun 1 virtuel maskine af gangen, derfor skal du åbne 2 instanser af vmware. En til Kali, og en til metasploitable_
4. Åben Wireshark og vælg at opfange trafik (Capture) fra det virtuelle netværksinterface der hedder “VMware Network Adapter VMnet8”
**Obs! trafikken du sniffer med Wireshark skal senere gemmes i en pcap file**
