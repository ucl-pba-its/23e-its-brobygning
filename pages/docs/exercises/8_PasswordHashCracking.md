---
hide:
  - footer
---

# Opgave 8 - Password hash cracking 

## Information
I denne opgave skal i finde password filen på den host i kompromitteret i øvelse 6.
Her efter skal i forsøge at cracke de hashed passwords som er opbevaret i filen.

## Instruktioner
1. Find ud af hvad password filen i Linux hedder (Brug evt. Google)
2. Anvend John the ripper til at omdanne det hashed password til plain text