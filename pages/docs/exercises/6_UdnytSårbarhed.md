---
hide:
  - footer
---

# Opgave 6 - Udnyt sårbarheder

## Information
I denne opgave skal sårbarh

Udnyt sårbarhed via Metasploit Framework Console for en given service (VNC, FTP, SMB etc.). I må IKKE fortælle de øvrige teams, hvad I har tænkt jer at gøre. 

## Instruktioner
Instruktioner er delt op i to dele. I den første del skal der ekskveres et angreb. Og i den anden del
skal netværk trafikken inspiceres.

### Del 1
**Sikr dig, at du optager din trafik i Wireshark, og at du gemmer denne trafik efter et givet angreb.**
1. Udnyt en sårbarhed med kommandoen use <noget fra listen>
2. Eksekver kommandoen `options`, og bemærk at _rhosts_ som er målet ip adresse ikke er sat.
3. Sæt målet ip adresse med kommandoen `set rhosts <metasploitable vm ip adresse>`.
4. Forsøg at udnytte sårbarheden med kommandoen `exploit`.
5. Hvis i er lykkes med at udnytte sårbarheden, har i nu adgang til metasploitable maskine. eksekver kommandoen `ls` for at se om det er tilfældet.
6. Gem den optaget netværkstrafik i en pcap file.


### del 2
1. Åben pcap filen i wireshark.
2. Vælg _Statistics->Protocol Hierachy_
3. højre klik op en af protokollerne, og vælg _Apply as filter -> Selected_, Hvad sker der?
4. Se om i kan finde noget af trafikken fra angrebet der blev udført i del 1.